#include "Grains.hpp"

#include <cmath>

#include "Windows.hpp"
#include "ofMath.h"
// #include "ofMath.h"

void Grain::start()
{
    alive = true;
    amplitude = ofClamp(
        centerAmplitude - ofRandom(td_one * centerAmplitude), 0, 1);

    float tempOctave = ofRandom(-1, 3) * td_one;
    speed = pow(2, tempOctave);
    duration = centerDuration * (1 + ofRandom(-td_one, td_one));
    float tempFreq = (1000 / duration);
    preValue = 0;
    phasor->setPhase(0);
    phasor->setFreq(tempFreq);

    xPos = ofClamp(xPosCenter + ofRandom((td_two * -0.5), (td_two * 0.5)),
        0, 1);
    yPos = ofClamp(yPosCenter + ofRandom((td_two * -0.5), (td_two * 0.5)),
        0, 3);

    for (int i = 0; i < 2; i++) {
        soundPlayers[(int)ofClamp(int(yPos) + i, 0, 3)]->setLoopEnabled(false);
        soundPlayers[(int)ofClamp(int(yPos) + i, 0, 3)]->play(speed);
        soundPlayers[(int)ofClamp(int(yPos) + i, 0, 3)]->setLocation(xPos);
    }
}

void Grain::process()
{
    float sampleOne, sampleTwo;

    if (alive) {
        soundPlayers[int(yPos)]->process();
        if (yPos < 3) {
            soundPlayers[int(yPos) + 1]->process();
        }
        sampleOne = soundPlayers[int(yPos)]->getSample() * (1 - (yPos - (int)yPos));

        if (yPos < 3) {
            sampleTwo = soundPlayers[int(yPos) + 1]->getSample() * (yPos - (int)yPos);
        } else {
            sampleTwo = sampleOne * (yPos - int(yPos));
        }

        phasor->process();
        currentSample = (sampleOne + sampleTwo) * amplitude * hannWindow(phasor->getSample());
        if (phasor->getSample() - preValue < 0) {
            currentSample = 0;
            end();
        }
        preValue = phasor->getSample();
    } else {
        currentSample = 0;
    }
}
