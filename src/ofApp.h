#pragma once

#include "Grains.hpp"
#include "ofMain.h"
#include "ofxATK.hpp"
#include "ofxGui.h"
#include "ofxNI2.h"
#include "ofxNiTE2.h"

#define MAX_GRAINS 128

//! Early draft of using the Kinect as a controller for grains. This does not yet represent the ideology of the usage of Kinect. This  is just a test to ensure that kinect can be used interactively to control granular synthesis.
class ofApp : public ofBaseApp {

public:
    void setup();
    void update();
    void draw();

    void audioOut(float* buffer, int bufferSize, int nChannels);
    void exit();

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    //------ Sound Defaults ---------------------------------
    ofSoundStream soundStream;
    int bufferSize;
    int sampleRate;

    //------ Kinect Defaults ---------------------------------
    //! Natural interaction API abstractions
    ofxNI2::Device device;
    //! An implementation of biometric data as body tracking for Kinect
    ofxNiTE2::UserTracker tracker;
    ofPixels depthPixels;
    ofTexture depthTexture;

    //------ Tracking ---------------------------------
    ofEasyCam cam;
    ofVec3f leftHandPos, rightHandPos;
    float lKneeRot, rKneeRot, lowestKnee, highestKnee, lnv;
    float rhx, lrhx, hrhx;

    //------ Granular  ---------------------------------
    ImpulseGenerator* impulse;
    Grain* grains[MAX_GRAINS];
    SoundFile* soundOne;
    SoundFile* soundTwo;
    SoundFile* soundThree;
    SoundFile* soundFour;
    Reverb* reverb;

    //------ Gui ----------------------------------------

    ofxPanel gui;
    ofxFloatSlider emissionRate;
    ofxFloatSlider amplitude;
    ofxFloatSlider duration;
    ofxFloatSlider xLocation;
    ofxFloatSlider yLocation;
    ofxFloatSlider leftOpen;
    ofxFloatSlider rightOpen;
    ofxFloatSlider reverbFeed;
    bool displayGUI;
};
