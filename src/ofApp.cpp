#include "ofApp.h"
#include "of3dUtils.h"
#include "ofMath.h"

//--------------------------------------------------------------
void ofApp::setup()
{
    sampleRate = 48000;
    bufferSize = 512;

    // Kinect----------------------------
    device.setLogLevel(OF_LOG_NOTICE);
    device.setup(0);
    tracker.setup(device);
    tracker.getSkeletonSmoothingFactor(0.8f);
    lKneeRot = 0.5;
    lowestKnee = 0.0;
    highestKnee = 0.0;
    lnv = 0.0;
    rhx = 0.0;
    lrhx = 0.0;
    hrhx = 0.0;

    // Grains ----------------------------
    impulse = new ImpulseGenerator(20);
    impulse->setPulseDeviation(1.0);

    soundOne = new SoundFile(ofToDataPath("trial.wav"));
    soundTwo = new SoundFile(ofToDataPath("trialDal.wav"));
    soundThree = new SoundFile(ofToDataPath("trialStu.wav"));
    soundFour = new SoundFile(ofToDataPath("trialCol.wav"));
    // reverb = new Reverb(0.1, 2000, 1.0);

    for (int i = 0; i < MAX_GRAINS; i++) {
        grains[i] = new Grain(soundOne, soundTwo, soundThree, soundFour);
    }

    //-------------- Gui Setup
    displayGUI = true;
    gui.setup();
    gui.add(emissionRate.setup("Emission", 40, 0.5, 256));
    gui.add(amplitude.setup("Amplitude", 0.0, 0.0, 1.0));
    gui.add(duration.setup("Duration", 100, 0.1, 200));
    gui.add(xLocation.setup("xLocation", 0.5, 0.0, 1.0));
    gui.add(yLocation.setup("yLocation", 0.0, 0.0, 3.0));
    gui.add(leftOpen.setup("leftOpen", 0.0, 0.0, 1.0));
    gui.add(rightOpen.setup("rightOpen", 0.0, 0.0, 1.0));
    // gui.add(reverbFeed.setup("reverb", 0.3, 0.0, 1.0));
    soundStream.setup(2, 0, ATKSettings::sampleRate, ATKSettings::bufferSize, 4);
}

//--------------------------------------------------------------
//! Use `ofxGui` temporarily as visual control, until personal implementation of `ImGui` is ready for deployment.
//! This callback loop exposes specific joint information from `NiTE` Api for kinect, to be tested as control for basic granualr synthesis controls.
void ofApp::update()
{
    device.update();

    float nUser = tracker.getNumUser();
    // Joints
    for (int i = 0; i < nUser; i++) {
        ofxNiTE2::User::Ref user = tracker.getUser(i);
        const ofxNiTE2::Joint& head = user->getJoint(nite::JOINT_HEAD);
        const ofxNiTE2::Joint& RHD = user->getJoint(nite::JOINT_RIGHT_HAND);
        const ofxNiTE2::Joint& LHD = user->getJoint(nite::JOINT_LEFT_HAND);
        const ofxNiTE2::Joint& LKN = user->getJoint(nite::JOINT_LEFT_KNEE);
        const ofxNiTE2::Joint& RKN = user->getJoint(nite::JOINT_RIGHT_KNEE);
        const ofxNiTE2::Joint& REL = user->getJoint(nite::JOINT_RIGHT_ELBOW);
        const ofxNiTE2::Joint& LEL = user->getJoint(nite::JOINT_LEFT_ELBOW);

        rightHandPos = RHD.getGlobalPosition();
        leftHandPos = LHD.getGlobalPosition();
        lKneeRot = ofMap(LKN.getHeadingDeg(), -6.0, 10, 0.05, 0.9);
        // lKneeRot = LKN.getHeadingDeg();
    }

    // cout << lKneeRot << endl;
    // if (lKneeRot < lowestKnee) {
    //   lowestKnee = lKneeRot;
    //   // cout << lowestKnee << endl;
    // }

    // if (lKneeRot > highestKnee) {
    //   highestKnee = lKneeRot;
    // }

    // lnv = ofMap(lKneeRot, lowestKnee, highestKnee, 0.0, 1.0, true);

    // if (rightHandPos.getNormalized().x < lrhx) {
    //   lrhx = rightHandPos.getNormalized().x;
    // }
    // if (rightHandPos.getNormalized().x > hrhx) {
    //   hrhx = rightHandPos.getNormalized().x;
    // }

    // rhx = ofMap(rightHandPos.getNormalized().x, lrhx, hrhx, 0.005, 0.95, true);
    // xLocation = rhx;
    // cout << lKneeRot << endl;
    // cout << rightHandPos.getNormalized().x << endl;
    amplitude = ofMap(rightHandPos.getNormalized().y, -0.3, 0.3, 0.005, 0.9, true);
    xLocation = ofMap(rightHandPos.getNormalized().x, 0.0, 0.2, 0.005, 0.9, true);

    if (lKneeRot >= 0 && lKneeRot <= 1) {
        leftOpen = lKneeRot;
    }

    // Assign variables to grains
    int n = 0;
    for (int i = 0; i < MAX_GRAINS; i++) {
        if (grains[i]->getAlive()) {
            n++;
        }
        grains[i]->setCenterAmplitude(amplitude);
        grains[i]->setCenterDuration(duration);
        grains[i]->setTendencyMaskTwo(leftOpen);
        grains[i]->setTendencyMaskOne(rightOpen);
        grains[i]->setXPos(xLocation);
        grains[i]->setYPos(yLocation);
    }
    // cout << "number active:" << n << endl;
    impulse->setFreq(emissionRate);
    impulse->setBurstMasking(rightOpen * 0.5);
    impulse->setPulseDeviation(rightOpen);
}

//--------------------------------------------------------------
void ofApp::draw()
{
    ofEnableAlphaBlending();
    cam.begin();
    ofDrawAxis(100);
    tracker.draw3D();

    cam.end();

    if (displayGUI) {
        soundOne->draw(20, 20, 400, 120);
        soundTwo->draw(20, 140, 400, 120);
        soundThree->draw(20, 260, 400, 120);
        soundFour->draw(20, 380, 400, 120);
        ofSetColor(ofColor::aliceBlue);
        float tempX = (xLocation * 400) + 20;
        float tempY = (yLocation * 120) + 20 + 60;
        float tempSize = fmax(leftOpen * 120, 10);
        ofDrawCircle(tempX, tempY, tempSize);
        ofSetDepthTest(false);
        gui.draw();
    }
}

//--------------------------------------------------------------
//! Fill an audio buffer with sample data from the granular synthesis process and pass it to the hardware sound card
void ofApp::audioOut(float* buffer, int bufferSize, int nChannels)
{
    for (int i = 0; i < bufferSize; i++) {
        float currentSample = 0;
        impulse->process();

        // trigger a new grain
        if (impulse->getSample() == 1) {
            for (int j = 0; j < MAX_GRAINS; j++) {
                if (grains[j]->getAlive() == false) {
                    grains[j]->start();
                    break;
                }
            }
        }

        for (int k = 0; k < MAX_GRAINS; k++) {
            grains[k]->process();
            currentSample += grains[k]->getSample();
        }
        // reverb->process(currentSample * reverbFeed);
        // currentSample += reverb->getSample();

        buffer[i * nChannels + 0] = currentSample;
        buffer[i * nChannels + 1] = currentSample;
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key) { }

//--------------------------------------------------------------
void ofApp::keyReleased(int key) { }

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) { }

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) { }

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) { }

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) { }

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) { }

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) { }

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) { }

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) { }

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) { }

//--------------------------------------------------------------
//! Close sound stream, kinect tracker and NiTE device on exit.
void ofApp::exit()
{
    soundStream.close();
    tracker.exit();
    device.exit();
}
