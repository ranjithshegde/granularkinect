#ifndef Grains_hpp
#define Grains_hpp

#include <stdio.h>

#include "SoundFile.hpp"
#include "SoundPlayer.hpp"
#include "ofMain.h"
#include "ofxATK.hpp"

//! A very simple implementation of a Granular engine. The concept & the visualization are borrowed from Aaron Anderson's impelementation found in
//! his extensions. His method will soon be replaced with my personal implementation `libsndfile` API (which is ongoing in a dev branch).
//! This implementation allows up to four different audio files to be processed. The controls are fairly simple as this is an ongoing work.
class Grain {
public:
    //! A simple constructor that allocates memory for new audio players.
    //! @param _soundOne A SoundFile object containing the link and properties to an audio file.
    //! @param _soundTwo A SoundFile object containing the link and properties to an audio file.
    //! @param _soundThree A SoundFile object containing the link and properties to an audio file.
    //! @param _soundFour A SoundFile object containing the link and properties to an audio file.
    Grain(SoundFile* _soundOne, SoundFile* _soundTwo, SoundFile* _soundThree,
        SoundFile* _soundFour)
    {
        soundOne = _soundOne;
        soundTwo = _soundTwo;
        soundThree = _soundThree;
        soundFour = _soundFour;

        soundPlayers[0] = new SoundPlayer(soundOne);
        soundPlayers[1] = new SoundPlayer(soundTwo);
        soundPlayers[2] = new SoundPlayer(soundThree);
        soundPlayers[3] = new SoundPlayer(soundFour);

        alive = false;
        phasor = new TPhasor(1);
    }

    //! Start playback of all files
    void start();

    //! Stop playback
    inline void end() { alive = false; }

    //! Process a single sample for all individual players. Apply windowing if set previously
    void process();

    //! get the currently processed sample.
    inline float getSample() { return currentSample; }

    //! Return whether the grain engine is active.
    inline bool getAlive() { return alive; }

    //! Duration can be set to be a random increment/decrement from a desired center value
    inline void setCenterDuration(float newDuration) { centerDuration = newDuration; }

    //! Amplitude can be set to be a random increment/decrement from a desired center value
    inline void setCenterAmplitude(float newAmplitude)
    {
        centerAmplitude = newAmplitude;
    }

    //! Set a center value as tendency mask for random controls. These apply to the values set by `Grain::setCenterDuration` and `Grain::setCenterAmplitude`
    inline void setTendencyMaskOne(float new_tdm) { td_one = new_tdm; }

    //! Set a center value as tendency mask for random controls. These apply to the values set by `Grain::setYPos` and `Grain::setXPos`
    inline void setTendencyMaskTwo(float new_tdm) { td_two = new_tdm; }

    //! Y coordinate (player index) can be set to be a random increment/decrement from a desired center value
    inline void setYPos(float newYPos) { yPosCenter = newYPos; }

    //! X coordinate (grain index) can be set to be a random increment/decrement from a desired center value
    inline void setXPos(float newXpos) { xPosCenter = newXpos; }

private:
    SoundFile* soundOne;
    SoundFile* soundTwo;
    SoundFile* soundThree;
    SoundFile* soundFour;

    SoundPlayer* soundPlayers[4];

    float currentSample;
    float centerDuration;
    float duration;
    float centerAmplitude;
    float amplitude;
    float speed;
    float position;
    float preValue;

    float td_two, td_one;
    float yPos;
    float xPos;
    float xPosCenter;
    float yPosCenter;

    TPhasor* phasor;
    bool alive;
};
#endif
