# Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`class `[`Grain`](#classGrain) | A very simple implementation of a Granular engine. The concept & the visualization are borrowed from Aaron Anderson's impelementation found in his extensions. His method will soon be replaced with my personal implementation `libsndfile` API (which is ongoing in a dev branch). This implementation allows up to four different audio files to be processed. The controls are fairly simple as this is an ongoing work.
`class `[`ofApp`](#classofApp) | Early draft of using the Kinect as a controller for grains. This does not yet represent the ideology of the usage of Kinect. This is just a test to ensure that kinect can be used interactively to control granular synthesis.

# class `Grain` 

A very simple implementation of a Granular engine. The concept & the visualization are borrowed from Aaron Anderson's impelementation found in his extensions. His method will soon be replaced with my personal implementation `libsndfile` API (which is ongoing in a dev branch). This implementation allows up to four different audio files to be processed. The controls are fairly simple as this is an ongoing work.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public inline  `[`Grain`](#classGrain_1a8afb89a490dfe1254fe584e0918d471c)`(SoundFile * _soundOne,SoundFile * _soundTwo,SoundFile * _soundThree,SoundFile * _soundFour)` | A simple constructor that allocates memory for new audio players. 
`public void `[`start`](#classGrain_1a6fd226540753fd958894c50f09d122a6)`()` | Start playback of all files.
`public inline void `[`end`](#classGrain_1a6f4a55b29b4032429c5a15c905570778)`()` | Stop playback.
`public void `[`process`](#classGrain_1a9eb4a4742ae4dee64ab0aaf3c0f1e66b)`()` | Process a single sample for all individual players. Apply windowing if set previously.
`public inline float `[`getSample`](#classGrain_1afaf1a28b6c9415569aadc0ff17f1b4c5)`()` | get the currently processed sample.
`public inline bool `[`getAlive`](#classGrain_1abb88ab6908e7c134bb4f5e6139e74e53)`()` | Return whether the grain engine is active.
`public inline void `[`setCenterDuration`](#classGrain_1a23c8fe0ccfe1bf684101965768fd62e9)`(float newDuration)` | Duration can be set to be a random increment/decrement from a desired center value.
`public inline void `[`setCenterAmplitude`](#classGrain_1a2463ab7c0938720d6e21315a0f08ee15)`(float newAmplitude)` | Amplitude can be set to be a random increment/decrement from a desired center value.
`public inline void `[`setTendencyMaskOne`](#classGrain_1a4406062519b62538a857eebddece2dea)`(float new_tdm)` | Set a center value as tendency mask for random controls. These apply to the values set by `[Grain::setCenterDuration](#classGrain_1a23c8fe0ccfe1bf684101965768fd62e9)` and `[Grain::setCenterAmplitude](#classGrain_1a2463ab7c0938720d6e21315a0f08ee15)`
`public inline void `[`setTendencyMaskTwo`](#classGrain_1accf3dd4a98305c4d2d193696b45448c1)`(float new_tdm)` | Set a center value as tendency mask for random controls. These apply to the values set by `[Grain::setYPos](#classGrain_1a99b594e93ccb9d7f29ac69e20f8a8de1)` and `[Grain::setXPos](#classGrain_1a168fa5d676346cf857de24cce873f589)`
`public inline void `[`setYPos`](#classGrain_1a99b594e93ccb9d7f29ac69e20f8a8de1)`(float newYPos)` | Y coordinate (player index) can be set to be a random increment/decrement from a desired center value.
`public inline void `[`setXPos`](#classGrain_1a168fa5d676346cf857de24cce873f589)`(float newXpos)` | X coordinate (grain index) can be set to be a random increment/decrement from a desired center value.

## Members

#### `public inline  `[`Grain`](#classGrain_1a8afb89a490dfe1254fe584e0918d471c)`(SoundFile * _soundOne,SoundFile * _soundTwo,SoundFile * _soundThree,SoundFile * _soundFour)` 

A simple constructor that allocates memory for new audio players. 
#### Parameters
* `_soundOne` A SoundFile object containing the link and properties to an audio file. 

* `_soundTwo` A SoundFile object containing the link and properties to an audio file. 

* `_soundThree` A SoundFile object containing the link and properties to an audio file. 

* `_soundFour` A SoundFile object containing the link and properties to an audio file.

#### `public void `[`start`](#classGrain_1a6fd226540753fd958894c50f09d122a6)`()` 

Start playback of all files.

#### `public inline void `[`end`](#classGrain_1a6f4a55b29b4032429c5a15c905570778)`()` 

Stop playback.

#### `public void `[`process`](#classGrain_1a9eb4a4742ae4dee64ab0aaf3c0f1e66b)`()` 

Process a single sample for all individual players. Apply windowing if set previously.

#### `public inline float `[`getSample`](#classGrain_1afaf1a28b6c9415569aadc0ff17f1b4c5)`()` 

get the currently processed sample.

#### `public inline bool `[`getAlive`](#classGrain_1abb88ab6908e7c134bb4f5e6139e74e53)`()` 

Return whether the grain engine is active.

#### `public inline void `[`setCenterDuration`](#classGrain_1a23c8fe0ccfe1bf684101965768fd62e9)`(float newDuration)` 

Duration can be set to be a random increment/decrement from a desired center value.

#### `public inline void `[`setCenterAmplitude`](#classGrain_1a2463ab7c0938720d6e21315a0f08ee15)`(float newAmplitude)` 

Amplitude can be set to be a random increment/decrement from a desired center value.

#### `public inline void `[`setTendencyMaskOne`](#classGrain_1a4406062519b62538a857eebddece2dea)`(float new_tdm)` 

Set a center value as tendency mask for random controls. These apply to the values set by `[Grain::setCenterDuration](#classGrain_1a23c8fe0ccfe1bf684101965768fd62e9)` and `[Grain::setCenterAmplitude](#classGrain_1a2463ab7c0938720d6e21315a0f08ee15)`

#### `public inline void `[`setTendencyMaskTwo`](#classGrain_1accf3dd4a98305c4d2d193696b45448c1)`(float new_tdm)` 

Set a center value as tendency mask for random controls. These apply to the values set by `[Grain::setYPos](#classGrain_1a99b594e93ccb9d7f29ac69e20f8a8de1)` and `[Grain::setXPos](#classGrain_1a168fa5d676346cf857de24cce873f589)`

#### `public inline void `[`setYPos`](#classGrain_1a99b594e93ccb9d7f29ac69e20f8a8de1)`(float newYPos)` 

Y coordinate (player index) can be set to be a random increment/decrement from a desired center value.

#### `public inline void `[`setXPos`](#classGrain_1a168fa5d676346cf857de24cce873f589)`(float newXpos)` 

X coordinate (grain index) can be set to be a random increment/decrement from a desired center value.

# class `ofApp` 

```
class ofApp
  : public ofBaseApp
```  

Early draft of using the Kinect as a controller for grains. This does not yet represent the ideology of the usage of Kinect. This is just a test to ensure that kinect can be used interactively to control granular synthesis.

## Summary

 Members                        | Descriptions                                
--------------------------------|---------------------------------------------
`public ofSoundStream `[`soundStream`](#classofApp_1a32f542fc195492272e0bb8bd4858c4fe) | 
`public int `[`bufferSize`](#classofApp_1af573fde175597807abfefd7b1c34c457) | 
`public int `[`sampleRate`](#classofApp_1abd83953a5dcff8f0deabbfc2a0170d98) | 
`public ofxNI2::Device `[`device`](#classofApp_1a388afd1e47d6d32ec022a8e221c28b0d) | Natural interaction API abstractions.
`public ofxNiTE2::UserTracker `[`tracker`](#classofApp_1aa4a2fdac22432dc1e14ea8b7cf191de5) | An implementation of biometric data as body tracking for Kinect.
`public ofPixels `[`depthPixels`](#classofApp_1adaadf7e4ea2cd5c6d1401e638252e7fe) | 
`public ofTexture `[`depthTexture`](#classofApp_1a223fac2934c133e8e25b44ea942237a3) | 
`public ofEasyCam `[`cam`](#classofApp_1a8fa3caf47e337c6cb16f362b415ba716) | 
`public ofVec3f `[`leftHandPos`](#classofApp_1ab0d7073d396015ce59c01279baa60859) | 
`public ofVec3f `[`rightHandPos`](#classofApp_1a546d9d98023931e0494cfece5f3ee697) | 
`public float `[`lKneeRot`](#classofApp_1ac10e13bca78fa7e3a77050b669b17b22) | 
`public float `[`rKneeRot`](#classofApp_1ad864a743b59fa3e963f334e50cb7f572) | 
`public float `[`lowestKnee`](#classofApp_1a999c93631d4035b1adab757c0fa1b3ac) | 
`public float `[`highestKnee`](#classofApp_1a9df52ef2a71a718120bb109c958a7e6b) | 
`public float `[`lnv`](#classofApp_1aa5da398ba6cfd92bcadf6797f641788d) | 
`public float `[`rhx`](#classofApp_1ace64c7b739657027318bf7feaf0b8291) | 
`public float `[`lrhx`](#classofApp_1afab7587d85857a14179006c9fd0abfdf) | 
`public float `[`hrhx`](#classofApp_1a2eedb4986d1e5cd1e9f923c58a511871) | 
`public ImpulseGenerator * `[`impulse`](#classofApp_1af0dc2da8ccd421ddeed6c27c64aa895b) | 
`public `[`Grain`](#classGrain)` * `[`grains`](#classofApp_1a35f81552f9e29308241602bc2374c34c) | 
`public SoundFile * `[`soundOne`](#classofApp_1ab685ef6b6558d88522f62020f4583c7c) | 
`public SoundFile * `[`soundTwo`](#classofApp_1ab9d90a270dc99421fdc2a8a1b56b74d7) | 
`public SoundFile * `[`soundThree`](#classofApp_1af09d1b51e5c01fdc3079ed7cead35f37) | 
`public SoundFile * `[`soundFour`](#classofApp_1a3afd8991345cf05a9d6ba247fef9cdd8) | 
`public Reverb * `[`reverb`](#classofApp_1ae3506735a9cd2e54a551fbcc37548674) | 
`public ofxPanel `[`gui`](#classofApp_1aefcf80606a04efd196a679cf4224a599) | 
`public ofxFloatSlider `[`emissionRate`](#classofApp_1aeb5a6d5a13e83d68378cde6c40ed5509) | 
`public ofxFloatSlider `[`amplitude`](#classofApp_1a2b23177ca1959d8bd49d9303aa3984a2) | 
`public ofxFloatSlider `[`duration`](#classofApp_1a95d3a825c6222aa3090d5982a41933db) | 
`public ofxFloatSlider `[`xLocation`](#classofApp_1a91224a63855a754017b326f9d25802c6) | 
`public ofxFloatSlider `[`yLocation`](#classofApp_1ae7b76e6ca85958de185f4ba8d6677287) | 
`public ofxFloatSlider `[`leftOpen`](#classofApp_1aef8e7e426166ad4012f0e2388bc811df) | 
`public ofxFloatSlider `[`rightOpen`](#classofApp_1a89f4e92608786125e249f98e497a6afc) | 
`public ofxFloatSlider `[`reverbFeed`](#classofApp_1ab9cc959009af618922b80f8cf5d97c64) | 
`public bool `[`displayGUI`](#classofApp_1ab254937b5a377068ff1eefa23c2519a9) | 
`public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` | 
`public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` | Use `ofxGui` temporarily as visual control, until personal implementation of `ImGui` is ready for deployment. This callback loop exposes specific joint information from `NiTE` Api for kinect, to be tested as control for basic granualr synthesis controls.
`public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` | 
`public void `[`audioOut`](#classofApp_1a7dac98eb535b3ac8ab8d1da7fa6f8caa)`(float * buffer,int bufferSize,int nChannels)` | Fill an audio buffer with sample data from the granular synthesis process and pass it to the hardware sound card.
`public void `[`exit`](#classofApp_1a41588341bbe9be134f6abdc2eb7cfd4c)`()` | Close sound stream, kinect tracker and NiTE device on exit.
`public void `[`keyPressed`](#classofApp_1a957d3197364bbac8e67eaa4f15b28ad3)`(int key)` | 
`public void `[`keyReleased`](#classofApp_1aa1503a87453bcfdd395fe4acca5d91a0)`(int key)` | 
`public void `[`mouseMoved`](#classofApp_1a158b41a606310db4633fdb817b21047c)`(int x,int y)` | 
`public void `[`mouseDragged`](#classofApp_1a1ec53d1be799dc275806ff6c6548cd83)`(int x,int y,int button)` | 
`public void `[`mousePressed`](#classofApp_1a2c2ea9c160231e55424dfd98466ef27d)`(int x,int y,int button)` | 
`public void `[`mouseReleased`](#classofApp_1aa3131f1554fc49eaa9ee0f284e48129b)`(int x,int y,int button)` | 
`public void `[`mouseEntered`](#classofApp_1aa54e1ffc660d8261617c369c9b29c432)`(int x,int y)` | 
`public void `[`mouseExited`](#classofApp_1ad31f79798b598551792dab6ba7c61fd1)`(int x,int y)` | 
`public void `[`windowResized`](#classofApp_1ae4dc1ec1513dcbe48bc78a5e4c3fac0f)`(int w,int h)` | 
`public void `[`dragEvent`](#classofApp_1aada5a79556321801567752a0e5a69bda)`(ofDragInfo dragInfo)` | 
`public void `[`gotMessage`](#classofApp_1a885672a72340a5e998af1d16718dc766)`(ofMessage msg)` | 

## Members

#### `public ofSoundStream `[`soundStream`](#classofApp_1a32f542fc195492272e0bb8bd4858c4fe) 

#### `public int `[`bufferSize`](#classofApp_1af573fde175597807abfefd7b1c34c457) 

#### `public int `[`sampleRate`](#classofApp_1abd83953a5dcff8f0deabbfc2a0170d98) 

#### `public ofxNI2::Device `[`device`](#classofApp_1a388afd1e47d6d32ec022a8e221c28b0d) 

Natural interaction API abstractions.

#### `public ofxNiTE2::UserTracker `[`tracker`](#classofApp_1aa4a2fdac22432dc1e14ea8b7cf191de5) 

An implementation of biometric data as body tracking for Kinect.

#### `public ofPixels `[`depthPixels`](#classofApp_1adaadf7e4ea2cd5c6d1401e638252e7fe) 

#### `public ofTexture `[`depthTexture`](#classofApp_1a223fac2934c133e8e25b44ea942237a3) 

#### `public ofEasyCam `[`cam`](#classofApp_1a8fa3caf47e337c6cb16f362b415ba716) 

#### `public ofVec3f `[`leftHandPos`](#classofApp_1ab0d7073d396015ce59c01279baa60859) 

#### `public ofVec3f `[`rightHandPos`](#classofApp_1a546d9d98023931e0494cfece5f3ee697) 

#### `public float `[`lKneeRot`](#classofApp_1ac10e13bca78fa7e3a77050b669b17b22) 

#### `public float `[`rKneeRot`](#classofApp_1ad864a743b59fa3e963f334e50cb7f572) 

#### `public float `[`lowestKnee`](#classofApp_1a999c93631d4035b1adab757c0fa1b3ac) 

#### `public float `[`highestKnee`](#classofApp_1a9df52ef2a71a718120bb109c958a7e6b) 

#### `public float `[`lnv`](#classofApp_1aa5da398ba6cfd92bcadf6797f641788d) 

#### `public float `[`rhx`](#classofApp_1ace64c7b739657027318bf7feaf0b8291) 

#### `public float `[`lrhx`](#classofApp_1afab7587d85857a14179006c9fd0abfdf) 

#### `public float `[`hrhx`](#classofApp_1a2eedb4986d1e5cd1e9f923c58a511871) 

#### `public ImpulseGenerator * `[`impulse`](#classofApp_1af0dc2da8ccd421ddeed6c27c64aa895b) 

#### `public `[`Grain`](#classGrain)` * `[`grains`](#classofApp_1a35f81552f9e29308241602bc2374c34c) 

#### `public SoundFile * `[`soundOne`](#classofApp_1ab685ef6b6558d88522f62020f4583c7c) 

#### `public SoundFile * `[`soundTwo`](#classofApp_1ab9d90a270dc99421fdc2a8a1b56b74d7) 

#### `public SoundFile * `[`soundThree`](#classofApp_1af09d1b51e5c01fdc3079ed7cead35f37) 

#### `public SoundFile * `[`soundFour`](#classofApp_1a3afd8991345cf05a9d6ba247fef9cdd8) 

#### `public Reverb * `[`reverb`](#classofApp_1ae3506735a9cd2e54a551fbcc37548674) 

#### `public ofxPanel `[`gui`](#classofApp_1aefcf80606a04efd196a679cf4224a599) 

#### `public ofxFloatSlider `[`emissionRate`](#classofApp_1aeb5a6d5a13e83d68378cde6c40ed5509) 

#### `public ofxFloatSlider `[`amplitude`](#classofApp_1a2b23177ca1959d8bd49d9303aa3984a2) 

#### `public ofxFloatSlider `[`duration`](#classofApp_1a95d3a825c6222aa3090d5982a41933db) 

#### `public ofxFloatSlider `[`xLocation`](#classofApp_1a91224a63855a754017b326f9d25802c6) 

#### `public ofxFloatSlider `[`yLocation`](#classofApp_1ae7b76e6ca85958de185f4ba8d6677287) 

#### `public ofxFloatSlider `[`leftOpen`](#classofApp_1aef8e7e426166ad4012f0e2388bc811df) 

#### `public ofxFloatSlider `[`rightOpen`](#classofApp_1a89f4e92608786125e249f98e497a6afc) 

#### `public ofxFloatSlider `[`reverbFeed`](#classofApp_1ab9cc959009af618922b80f8cf5d97c64) 

#### `public bool `[`displayGUI`](#classofApp_1ab254937b5a377068ff1eefa23c2519a9) 

#### `public void `[`setup`](#classofApp_1af68eaa1366244f7a541cd08e02199c12)`()` 

#### `public void `[`update`](#classofApp_1afef41ea4aee5a22ea530afba33ae7a7b)`()` 

Use `ofxGui` temporarily as visual control, until personal implementation of `ImGui` is ready for deployment. This callback loop exposes specific joint information from `NiTE` Api for kinect, to be tested as control for basic granualr synthesis controls.

#### `public void `[`draw`](#classofApp_1a75dd45437b9e317db73d8daef1ad49f8)`()` 

#### `public void `[`audioOut`](#classofApp_1a7dac98eb535b3ac8ab8d1da7fa6f8caa)`(float * buffer,int bufferSize,int nChannels)` 

Fill an audio buffer with sample data from the granular synthesis process and pass it to the hardware sound card.

#### `public void `[`exit`](#classofApp_1a41588341bbe9be134f6abdc2eb7cfd4c)`()` 

Close sound stream, kinect tracker and NiTE device on exit.

#### `public void `[`keyPressed`](#classofApp_1a957d3197364bbac8e67eaa4f15b28ad3)`(int key)` 

#### `public void `[`keyReleased`](#classofApp_1aa1503a87453bcfdd395fe4acca5d91a0)`(int key)` 

#### `public void `[`mouseMoved`](#classofApp_1a158b41a606310db4633fdb817b21047c)`(int x,int y)` 

#### `public void `[`mouseDragged`](#classofApp_1a1ec53d1be799dc275806ff6c6548cd83)`(int x,int y,int button)` 

#### `public void `[`mousePressed`](#classofApp_1a2c2ea9c160231e55424dfd98466ef27d)`(int x,int y,int button)` 

#### `public void `[`mouseReleased`](#classofApp_1aa3131f1554fc49eaa9ee0f284e48129b)`(int x,int y,int button)` 

#### `public void `[`mouseEntered`](#classofApp_1aa54e1ffc660d8261617c369c9b29c432)`(int x,int y)` 

#### `public void `[`mouseExited`](#classofApp_1ad31f79798b598551792dab6ba7c61fd1)`(int x,int y)` 

#### `public void `[`windowResized`](#classofApp_1ae4dc1ec1513dcbe48bc78a5e4c3fac0f)`(int w,int h)` 

#### `public void `[`dragEvent`](#classofApp_1aada5a79556321801567752a0e5a69bda)`(ofDragInfo dragInfo)` 

#### `public void `[`gotMessage`](#classofApp_1a885672a72340a5e998af1d16718dc766)`(ofMessage msg)` 

Generated by [Moxygen](https://sourcey.com/moxygen)