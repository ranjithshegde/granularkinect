# A Kinect test for audio

_Notice: This project is very experimental. The API is subject to change with
noitce whereas the code is subject to change without Notice_

This is a simple sensitivity caliberation test for Kinect V2 for its
effectiveness in granular synthesis. This is _not_ a usage example.

For a quickstart documentation, follow this file: [API](API.md)

For a more complete documentation, please click
[here](https://ranjithshegde.gitlab.io/granularkinect)
